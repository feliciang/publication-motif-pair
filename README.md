##README

This page contains R source code for the PWM similarity clustering described in [Ng et al. Nucleic Acids Res (2014)](http://www.ncbi.nlm.nih.gov/pubmed/25428352).

To get started, see the example clustering procedure in 'run.R' performed on a small subset of PWMs (provided in the 'data' folder). The source code provides 2 metric for comparing PWMs - Euclidean distance (as described in the paper) and Pearson correlation.

If you have any questions, please contact:

Felicia Ng fn231 [at] cam.ac.uk