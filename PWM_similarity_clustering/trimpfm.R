#===============================================================================
#
#  FUNCTION:    trimpfm
#
#  DESCRIPTION: Script to trim flanking regions of a frequency matrix with low
#				IC to obtain a 'core' motif. New PWM must have at least a user
#				defined length. The procedure is as follows: reads in .pfm or 
#				.pwm files, converts PFM to PWM if input is .pfm, trim low IC 
#				columns and then writes PWM to file.
#				
#  ARGUMENTS:
#  icthr:   
#    information content threshold
#  len:   
#    minimum length of 'core' motif
#  pwmdir:
#	 path to PWMs
#  outdir:
#    path to where results will be saved
#
#  RETURN:  	One .pwm file for each input file
#
#  AUTHOR:      Felicia Ng (fn231)
#  INST: 		Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     2 Aug 2013
#===============================================================================

trimpfm <- function(icthr=0.5, len=4, pwmdir=NULL, outdir=NULL) {
	
	bg=c(0.25, 0.25, 0.25, 0.25)
	
	if (is.null(pwmdir)) {
		stop("pwmdir not specified!")	
	}
	
	if (!is.null(outdir) & !exists(outdir)) {
		dir.create(outdir)
	}else if (is.null(outdir)) {
		stop("outdir not specified!")	
	}
	
	currdir <- getwd()
	setwd(pwmdir)
	inputfilenames <- list.files(pattern=".pwm")
	
	for (i in 1:length(inputfilenames)) {
		
		pwm <- read.table(inputfilenames[i])
		
		logIC <- apply(log2(pwm/bg)*pwm, 2, function(x) sum(na.omit(x)))
		idxhi <- which(logIC>=icthr)
		
		if (length(idxhi)>0) {
			start <- min(idxhi)
			end <- max(idxhi)
			newpwm <- as.matrix(pwm[,start:end])
			
			if (ncol(newpwm)>=len) {
				newpwm <- round(newpwm, 6)
				outfilename <- paste(outdir, "/", inputfilenames[i], sep="")
				write.table(format(newpwm, digits=5), file=outfilename, sep="\t", row.names=F, col.names=F, quote=F)	
			}
		}
		
	}
	
	setwd(currdir)
}