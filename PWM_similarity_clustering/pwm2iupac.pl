#!/usr/bin/perl

use strict;
use warnings;
use Bio::Matrix::PSM::SiteMatrix;

my $dir = $ARGV[0];
chomp $dir;
opendir(DIR, $dir) or die $!;

while (my $pwmfile = readdir(DIR)) {
	
	# Ignore non PWM files
	next if ($pwmfile !~ m/.pwm$/);
	
    my @data = get_file_data($pwmfile);
    my $row = 1;
    my @a = ();
    my @c = ();
    my @g = ();
    my @t = ();

    foreach my $line (@data) {

	    my $motifname;
	
        if ($row==1) {
		    @a = split ( "\t", $line);
	    }elsif ($row==2) {
	        @c = split ( "\t", $line);
	    }elsif ($row==3) {
	        @g = split ( "\t", $line);
        }elsif ($row==4) {
            @t = split ( "\t", $line);
		    my $iupac = getIUPAC(\@a, \@c, \@g, \@t);
		    print $iupac, "\n";
		    $row = 1;
		    @a = ();
		    @c = ();
		    @g = ();
		    @t = ();
	    }

	    $row++;
    }

}

sub getIUPAC {
	
	my ($a, $c, $g, $t, $mid) = @_;
	
	my %param=(-pA=>$a,-pC=>$c,-pG=>$g,-pT=>$t,-id=>$mid);
	my $site=Bio::Matrix::PSM::SiteMatrix->new(%param);
	
	my $iupac=$site->IUPAC;
	return $iupac;
	
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;

}

exit;